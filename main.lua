require "CiderDebugger";
display.setStatusBar( display.HiddenStatusBar )

local widget = require "widget"
local physics = require "physics"
physics.start()
--physics.setDrawMode( "hybrid" )


-- GLOBAL VARIABLES
H = display.contentHeight
W = display.contentWidth
centerHight = H * .50
centerWidth = W * .50
rnd = math.random

-- LOCAL VARIABLES
local gifts = {}
local coal = {}
local liveImg = {}
local idxGift = 1
local idxCoal = 1
local score = 0
local lives = 3
local scoreTitle
local scoreText
local nextGift
local timerGift
local timerCoal
local isRainingCoal = false
local isAudioOn = true
local isSfxOn = true
local squareShape = { -30,-50, 30,-50, 30,-30, -30,-30 }
local sndMusicIntro = audio.loadStream("sound/intro.mp3")
local sndMusicLoop = audio.loadStream("sound/loop.mp3")


local spriteSpeedCoal = 2000
local sheetDataCoal = {
    width = 64,
    height = 64,
    numFrames = 16
}
local imageSheetCoal = graphics.newImageSheet("img/coalGray.png", sheetDataCoal)
local sequenceDataCoal =
{
    { name="coal", start=1, count=16, time=spriteSpeedCoal },
}

local spriteSpeedDeer = 500
local sheetDataDeer = {
    width = 150,
    height = 128,
    numFrames = 4
}
local imageSheetDeer = graphics.newImageSheet("img/elvenao.png", sheetDataDeer)
local sequenceDataDeer =
{
    { name="deer", start=1, count=4, time=spriteSpeedDeer },
}


-- FORWARD DECLARATIONS
local mainGame
local boyMove
local carbonRain
local santa
local livesUpdate
local gameOver
local restartGame

function restartGame()
    local function onCompleteAlert( event )
        local i = event.index
        if 1 == i then
            -- Do nothing; dialog will simply dismiss
        elseif 2 == i then
            mainGame()
        end
        native.showAlert("Bad Kid!", "Do you want to try again?", {"No", "Yes"}, onCompleteAlert)
    end
end

function gameOver()
    
--    for i = #coal, 1, -1 do
--        coal[i]:removeSelf()
--        coal[i] = nil
--    end
    
    timer.cancel(timerCoal)
    timer.cancel(timerGift)
    timer.cancel(timerSanta)
    
    restartGame()
end

function livesUpdate()
    if lives == 0 then
        gameOver()
    end
    
    for i = #liveImg, 1, -1 do
        liveImg[i]:removeSelf()
        liveImg[i] = nil
    end
    
    for i = 1, lives do
        liveImg[i] = display.newImageRect("img/avatar.png", 15, 25)
        liveImg[i].x = 220 + (i * 25)
        liveImg[i].y = 20
        liveImg[i].id = "liveImg" .. i
    end
end

function carbonRain()
    local function createCoal( event )
        local count = event.count
        
        coal[idxCoal] = display.newSprite( imageSheetCoal, sequenceDataCoal )
        coal[idxCoal].id = "coal"
        coal[idxCoal].x = rnd(20, 300)
        coal[idxCoal].y = -20
        coal[idxCoal]:scale(.5, .5)
        coal[idxCoal]:setSequence("coal")
        coal[idxCoal]:play()
        
        physics.addBody( coal[idxCoal], { density = 1.0, friction = 1.0, bounce = 0.0, radius = 13 } )

        idxCoal = idxCoal + 1
        
        if count >= 15 then
            isRainingCoal = false
            timer.resume(timerGift)
        end
    end
    timerCoal = timer.performWithDelay( rnd(250, 1000), createCoal, 15 )
end

function santa()
    local santaGroup = display.newGroup()
    local santa = display.newImageRect("img/santa.png", 75, 65)
    santa:setReferencePoint(display.CenterLeftReferencePoint)
    santa.x = -150
    santa.y = 80
    santaGroup:insert(santa)
    
    local deer = display.newSprite( imageSheetDeer, sequenceDataDeer )
    deer:setReferencePoint(display.CenterLeftReferencePoint)
    deer.id = "deer"
    deer.x = santa.x + 80
    deer.y = santa.y
    deer:scale(.5, .5)
    deer:setSequence("deer")
    deer:play()
    santaGroup:insert(deer)
    
    local function santaRestart( event )
        santaGroup.x = -150
    end
    transition.to(santaGroup, {time = 3500, x = 470, onComplete = santaRestart })

    
end

function boyMove( event )
    local phase = event.phase
    local obj = event.target
    
    if phase == "began" then
        display.getCurrentStage():setFocus( obj )
        obj.isFocus = true
        obj.markX = obj.x    -- store x location of object
    elseif obj.isFocus then
        if phase == "moved" then
            local x = (event.x - event.xStart) + obj.markX

            obj.x = x    -- move object based on calculations above
        elseif phase == "ended" then
            display.getCurrentStage():setFocus( nil )
            obj.isFocus = nil
        end
    end
    
    return true
    
end


function onGiftCollision( event )
    local phase = event.phase
    local obj1 = event.object1
    local obj2 = event.object2
    
    if ( phase == "began" ) then
        if obj1.id == "boy" and obj2.id == "gift" then
            if nextGift.color == obj2.color then
                if obj2.color == "red" then
                    nextGift:setFillColor(255,0,0)
                    nextGift.color = "red"
                elseif obj2.color == "green" then
                    nextGift:setFillColor(0,255,0)
                    nextGift.color = "green"
                elseif obj2.color == "blue" then
                    nextGift:setFillColor(0,0,255)
                    nextGift.color = "blue"
                elseif obj2.color == "yellow" then
                    nextGift:setFillColor(255,255,0)
                    nextGift.color = "yellow"
                end
                score = score + 1
                scoreText.text = score
                scoreText:setReferencePoint( display.CenterLeftReferencePoint )
                scoreText.x = scoreTitle.x + 110
                scoreText.y = scoreTitle.y
            elseif nextGift.color == obj2.bow.color then
                if obj2.color == "red" then
                    nextGift:setFillColor(255,0,0)
                    nextGift.color = "red"
                elseif obj2.color == "green" then
                    nextGift:setFillColor(0,255,0)
                    nextGift.color = "green"
                elseif obj2.color == "blue" then
                    nextGift:setFillColor(0,0,255)
                    nextGift.color = "blue"
                elseif obj2.color == "yellow" then
                    nextGift:setFillColor(255,255,0)
                    nextGift.color = "yellow"
                end
                score = score + 1
                scoreText.text = score
                scoreText:setReferencePoint( display.CenterLeftReferencePoint )
                scoreText.x = scoreTitle.x + 110
                scoreText.y = scoreTitle.y
            else
                if not isRainingCoal then
                    print("Carbon Rain")
                    isRainingCoal = true
                    timer.pause(timerGift)
                    carbonRain()
                end
            end
            obj2.bow:removeSelf()
            obj2.bow = nil
            obj2:removeSelf()
            obj2 = nil
        elseif obj1.id == "ground" and obj2.id == "gift" then
            obj2.bow:removeSelf()
            obj2.bow = nil
            obj2:removeSelf()
            obj2 = nil
        elseif obj1.id == "boy" and obj2.id == "coal" then
            print("boy is dead")
            lives = lives - 1
            print("lives: ".. lives)
            livesUpdate()
            obj2:removeSelf()
            obj2 = nil
        elseif obj1.id == "ground" and obj2.id == "coal" then
            obj2:removeSelf()
            obj2 = nil
        end        
    end
end

function mainGame()
    local function startLoop()
        audio.play(sndMusicLoop, { loops=-1 })
    end
    audio.play(sndMusicIntro, { onComplete=startLoop })

    local background = display.newImageRect("img/bg.png", 320, 480)
    background:setReferencePoint(display.TopLeftReferencePoint)
    background.x = 0
    background.y = 0
    
    scoreTitle = display.newText("Score:", 0, 0, native.systemFontBold, 32)
    scoreTitle:setTextColor(255,255,255)
    scoreTitle:setReferencePoint( display.CenterLeftReferencePoint )
    scoreTitle.x = 10
    scoreTitle.y = 20
    
    scoreText = display.newText(tostring(score), 0, 0, native.systemFontBold, 32)
    scoreText:setTextColor(255,255,255)
    scoreText:setReferencePoint( display.CenterLeftReferencePoint )
    scoreText.x = scoreTitle.x + 110
    scoreText.y = scoreTitle.y

    local ground = display.newRect(0, 479, 320, 5)
    ground.id = "ground"
    ground:setFillColor(153,76,0)
    ground.alpha = .01
    physics.addBody( ground, "static", { density = 1.0, friction = 0.3, bounce = 0.2 } )

    local boy = display.newImageRect("img/avatar.png", 60, 100)
    boy.x = centerWidth
    boy.y = 420
    boy.id = "boy"
    physics.addBody( boy, "static", { density = 1.0, friction = 1.0, bounce = 0, shape = squareShape } )
    boy:addEventListener("touch", boyMove)
    
    nextGift = display.newRect(10, 430, 45, 30)
    nextGift:setFillColor(255,0,0)
    nextGift.color = "red"
    
    local function createGift()
        local colorGift = rnd(1, 4)
        local colorBow = rnd(1, 4)
        
        --gifts[idxGift] = display.newRect(rnd(20, 300), -20, 45, 30)
        gifts[idxGift] = display.newRect(rnd(20, 300), -20, 45, 30)
        gifts[idxGift].id = "gift"
        if colorGift == 1 then
            gifts[idxGift]:setFillColor(255,0,0)
            gifts[idxGift].color = "red"
        elseif colorGift == 2 then
            gifts[idxGift]:setFillColor(0,255,0)
            gifts[idxGift].color = "green"
        elseif colorGift == 3 then
            gifts[idxGift]:setFillColor(0,0,255)
            gifts[idxGift].color = "blue"
        elseif colorGift == 4 then
            gifts[idxGift]:setFillColor(255,255,0)
            gifts[idxGift].color = "yellow"
        end
        
        if colorBow == 1 then
            gifts[idxGift].bow = display.newImageRect("img/bowR.png", 25, 25)
            gifts[idxGift].bow.color = "red"
        elseif colorBow == 2 then
            gifts[idxGift].bow = display.newImageRect("img/bowG.png", 25, 25)
            gifts[idxGift].bow.color = "green"
        elseif colorBow == 3 then
            gifts[idxGift].bow = display.newImageRect("img/bowB.png", 25, 25)
            gifts[idxGift].bow.color = "blue"
        elseif colorBow == 4 then
            gifts[idxGift].bow = display.newImageRect("img/bowY.png", 25, 25)
            gifts[idxGift].bow.color = "yellow"
        end
        
        gifts[idxGift].bow.x = gifts[idxGift].x
        gifts[idxGift].bow.y = gifts[idxGift].y - 15

        
        physics.addBody( gifts[idxGift], { density = 1.0, friction = 1.0, bounce = 0.0 } )
        physics.addBody( gifts[idxGift].bow, { density = 1.0, friction = 1.0, bounce = 0.0 } )
        physics.newJoint( "weld", gifts[idxGift], gifts[idxGift].bow, 0, 0 )

        idxGift = idxGift+1
    end
    
    livesUpdate()
    
    timerGift = timer.performWithDelay( rnd(1000, 2000), createGift, 0 )
    
    santa()
    timerSanta = timer.performWithDelay( 6000, santa, -1 )
end

mainGame()
Runtime:addEventListener( "collision", onGiftCollision )
